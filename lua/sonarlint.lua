local M = {}

M.client_id = {}

local java = require("sonarlint.java")
local utils = require("sonarlint.utils")

local update_settings
if vim.fn.has("nvim-0.10") == 1 then
   function update_settings(client, settings)
      client.settings = vim.tbl_deep_extend("force", client.settings, settings)
   end
else
   function update_settings(client, settings)
      client.config.settings = vim.tbl_deep_extend("force", client.config.settings, settings)
   end
end

local function init_with_config_notify(original_init, original_settings)
   return function(...)
      local client = select(1, ...)
      update_settings(client, original_settings)

      if original_init then
         original_init(...)
      end
   end
end

local function find_root_dir(config, bufnr)
   return (
      config.root_dir
      or vim.fs.dirname(vim.fs.find({ ".git" }, { path = vim.api.nvim_buf_get_name(bufnr), upward = true })[1])
      or vim.fn.getcwd()
   )
end

local function start_sonarlint_lsp(user_config, root_dir)
   local config = vim.tbl_deep_extend("keep", user_config, {
      root_dir = root_dir,
      capabilities = vim.lsp.protocol.make_client_capabilities(),
      settings = { sonarlint = { rules = vim.empty_dict() } },
   })

   config.name = "sonarlint.nvim"

   config.init_options = vim.tbl_deep_extend("keep", config.init_options or {}, {
      productKey = "sonarlint.nvim",
      productName = "SonarLint.nvim",
      productVersion = "0.1.0",
      -- TODO: get workspace name
      workspaceName = "some-workspace-name",
      firstSecretDetected = false,
      showVerboseLogs = true,
      platform = vim.loop.os_uname().sysname,
      architecture = vim.loop.os_uname().machine,
   })

   config.handlers = {}

   config.handlers["sonarlint/isOpenInEditor"] = function(_, uri)
      return utils.is_open_in_editor(uri[1])
   end
   config.handlers["sonarlint/shouldAnalyseFile"] = function(_, uri)
      return {
         shouldBeAnalysed = utils.is_open_in_editor(uri.uri),
      }
   end

   config.handlers["sonarlint/readyForTests"] = function()
      vim.notify("SonarQube language server is ready.", vim.log.levels.INFO)
   end

   config.handlers["sonarlint/isIgnoredByScm"] = require("sonarlint.scm").is_ignored_by_scm

   config.handlers["sonarlint/listFilesInFolder"] = require("sonarlint.autobinding").list_autobinding_files_in_folder
   config.handlers["sonarlint/filterOutExcludedFiles"] = function(_, params)
      return params
   end

   config.handlers["sonarlint/needCompilationDatabase"] = function(_, _, ctx)
      local locations = vim.fs.find("compile_commands.json", {
         upward = true,
         path = vim.fs.dirname(vim.api.nvim_buf_get_name(0)),
      })
      if #locations > 0 then
         local client = vim.lsp.get_client_by_id(ctx.client_id)
         update_settings(client, {
            sonarlint = {
               pathToCompileCommands = locations[1],
            },
         })
         client.notify("workspace/didChangeConfiguration", {
            settings = {},
         })
      else
         vim.notify_once(
            "Couldn't find compile_commands.json. Make sure it exists in a parent directory.",
            vim.log.levels.ERROR
         )
      end
   end

   config.handlers["sonarlint/showRuleDescription"] = require("sonarlint.rules").show_rule_handler

   -- TODO: persist settings
   config.commands = {
      ["SonarLint.DeactivateRule"] = function(action, ctx)
         local rule = action.arguments[1]
         if rule ~= nil then
            local client = vim.lsp.get_client_by_id(ctx.client_id)
            update_settings(client, {
               sonarlint = {
                  rules = {
                     [rule] = {
                        level = "off",
                     },
                  },
               },
            })
            client.notify("workspace/didChangeConfiguration", {
               settings = {},
            })
         end
      end,
      ["SonarLint.ShowAllLocations"] = function(result)
         local list = {}
         for _, arg in ipairs(result.arguments) do
            local bufnr = vim.uri_to_bufnr(arg.fileUri)

            for _, flow in ipairs(arg.flows) do
               for _, location in ipairs(flow.locations) do
                  local text_range = location.textRange

                  table.insert(list, {
                     bufnr = bufnr,
                     lnum = text_range.startLine,
                     col = text_range.startLineOffset,
                     end_lnum = text_range.endLine,
                     end_col = text_range.endLineOffset,
                     text = arg.message,
                  })
               end
            end
         end

         vim.fn.setqflist(list, "r")
         vim.cmd("copen")
      end,
   }

   config.on_init = init_with_config_notify(config.on_init, config.settings)
   return vim.lsp.start_client(config)
end

function M.setup(config)
   if not config.filetypes then
      vim.notify("Please, provide filetypes as a list of filetype.", vim.log.levels.WARN)
      return
   end

   local pattern = {}
   local attach_to_jdtls = false
   for _, filetype in ipairs(config.filetypes) do
      if filetype == "java" then
         attach_to_jdtls = true
      end
      table.insert(pattern, filetype)
   end

   config.server.on_init = function(client, _)
      if attach_to_jdtls then
         client.config.handlers["sonarlint/getJavaConfig"] = java.get_java_config_handler
         java.init_config(client)
      end
   end

   vim.api.nvim_create_autocmd("FileType", {
      pattern = table.concat(pattern, ","),
      callback = function(e)
         local bufnr = e.buf
         local root_dir = find_root_dir(config, bufnr)
         if not M.client_id[root_dir] then
            M.client_id[root_dir] = start_sonarlint_lsp(config.server, root_dir)
         end
         vim.lsp.buf_attach_client(e.buf, M.client_id[root_dir])
      end,
   })

   vim.api.nvim_create_autocmd({ "BufEnter", "LspAttach" }, {
      callback = require("sonarlint.scm").check_git_branch_and_notify_lsp,
   })
end

return M

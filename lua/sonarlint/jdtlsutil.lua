local M = {}

---@diagnostic disable-next-line: deprecated
local get_clients = vim.lsp.get_clients or vim.lsp.get_active_clients

---this command is copied from nvim-jdtls utils.lua package
function M.execute_command(command, callback, bufnr)
   local clients = {}
   local candidates = get_clients({ bufnr = bufnr })
   for _, c in pairs(candidates) do
      local command_provider = c.server_capabilities.executeCommandProvider
      local commands = type(command_provider) == "table" and command_provider.commands or {}
      if vim.tbl_contains(commands, command.command) then
         table.insert(clients, c)
      end
   end
   local num_clients = vim.tbl_count(clients)
   if num_clients == 0 then
      if bufnr then
         -- User could've switched buffer to non-java file, try all clients
         return M.execute_command(command, callback, nil)
      else
         vim.notify("No LSP client found that supports " .. command.command, vim.log.levels.ERROR)
         return
      end
   end

   if num_clients > 1 then
      vim.notify(
         "Multiple LSP clients found that support "
            .. command.command
            .. " you should have at most one JDTLS server running",
         vim.log.levels.WARN
      )
   end

   local co
   if not callback then
      co = coroutine.running()
      if co then
         callback = function(err, resp)
            coroutine.resume(co, err, resp)
         end
      end
   end
   clients[1].request("workspace/executeCommand", command, callback, bufnr)
   if co then
      return coroutine.yield()
   end
end

return M

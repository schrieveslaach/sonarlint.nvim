local M = {}

function M.show_rule_handler(err, result, context)
   local buf = vim.api.nvim_create_buf(false, true)
   vim.api.nvim_set_option_value("filetype", "markdown", { buf = buf })

   local htmlDescription = result.htmlDescription
   local markdown_lines = {}
   if htmlDescription == nil or htmlDescription == "" then
      for _, htmlDescriptionTab in ipairs(result.htmlDescriptionTabs) do
         local ruleDescriptionTabHtmlContent = htmlDescriptionTab.ruleDescriptionTabContextual.htmlContent
            or htmlDescriptionTab.ruleDescriptionTabNonContextual.htmlContent
         vim.list_extend(
            markdown_lines,
            vim.lsp.util.convert_input_to_markdown_lines(
               "## " .. htmlDescriptionTab.title .. "\n\n" .. ruleDescriptionTabHtmlContent .. "\n\n"
            )
         )
      end
   else
      markdown_lines = vim.lsp.util.convert_input_to_markdown_lines(htmlDescription)
   end

   vim.api.nvim_buf_set_lines(buf, -2, -1, false, markdown_lines)
   vim.api.nvim_set_option_value("readonly", true, { buf = buf })
   vim.api.nvim_set_option_value("modifiable", false, { buf = buf })

   vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = buf, silent = true })

   vim.cmd("vsplit")
   local win = vim.api.nvim_get_current_win()
   vim.api.nvim_win_set_buf(win, buf)
end

function M.list_all_rules()
   local clients = vim.lsp.get_clients({ name = "sonarlint.nvim", bufnr = 0 })

   if #clients ~= 1 then
      vim.notify("Found more then one attached Sonarlint client. That shouldn't be possible", vim.log.levels.ERROR)
      return
   end

   clients[1].request("sonarlint/listAllRules", {}, function(err, result)
      if err then
         vim.notify("Cannot request the list of rules: " .. err, vim.log.levels.ERROR)
         return
      end
      local buf = vim.api.nvim_create_buf(false, true)

      for language, rules in pairs(result) do
         vim.api.nvim_buf_set_lines(buf, -1, -1, false, { "# " .. language, "" })

         for _, rule in ipairs(rules) do
            local line = { " - ", rule.key, ": ", rule.name }

            if rule.activeByDefault then
               line[#line + 1] = " (active by default)"
            end
            vim.api.nvim_buf_set_lines(buf, -1, -1, false, { table.concat(line, "") })
         end
         vim.api.nvim_buf_set_lines(buf, -1, -1, false, { "" })
      end

      vim.api.nvim_set_option_value("filetype", "markdown", { buf = buf })
      vim.api.nvim_set_option_value("readonly", true, { buf = buf })
      vim.api.nvim_set_option_value("modifiable", false, { buf = buf })
      vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = buf, silent = true })

      vim.cmd("vsplit")
      local win = vim.api.nvim_get_current_win()
      vim.api.nvim_win_set_buf(win, buf)
   end)
end

vim.api.nvim_create_user_command("SonarlintListRules", M.list_all_rules, {})

return M

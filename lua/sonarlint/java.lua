local M = {}

M._initialized_clients = {}
M._server_ready = {}
M._co = {}
M._classpath_change_listener = {}

-- https://github.com/redhat-developer/vscode-java/blob/38c8b582b40db9644696924f899c73a3251563f4/src/protocol.ts#L59
M.EventType = {
   classpathUpdated = 100,
   projectsImported = 200,
   projectsDeleted = 210,
   incompatibleGradleJdkIssue = 300,
   upgradeGradleWrapper = 400,
   sourceInvalidated = 500,
}

local utils = require("sonarlint.utils")

-- https://github.com/redhat-developer/vscode-java/blob/38c8b582b40db9644696924f899c73a3251563f4/src/standardLanguageClient.ts#L192
function M.handle_event_notify(_, msg, ctx)
   if msg.eventType == M.EventType.classpathUpdated then
      local jdtls_client = vim.lsp.get_client_by_id(ctx.client_id)

      local sonarlint = utils.get_sonarlint_client(jdtls_client)
      if not sonarlint then
         return
      end

      sonarlint.notify("sonarlint/didClasspathUpdate", {
         projectUri = msg.data,
      })
   end
end

function M.install_classpath_listener(sonarlint_client)
   if not M._classpath_change_listener[sonarlint_client.id] then
      return
   end
   M._classpath_change_listener[sonarlint_client.id] = true

   local client = utils.get_jdtls_client(sonarlint_client)
   if not client then
      return
   end

   if client.config.handlers["language/eventNotification"] then
      local old_handler = client.config.handlers["language/eventNotification"]
      client.config.handlers["language/eventNotification"] = function(...)
         old_handler(...)
         M.handle_event_notify(...)
      end
   else
      client.config.handlers["language/eventNotification"] = M.handle_event_notify
   end
end

function M.handle_service_ready(_, msg, ctx)
   if "ServiceReady" == msg.type then
      local jdtls_client = vim.lsp.get_client_by_id(ctx.client_id)
      local sonarlint_client = utils.get_sonarlint_client(jdtls_client)
      if not sonarlint_client then
         vim.notify("Cannot find sonarlint client that matches to Java LSP client", vim.log.levels.ERROR)
         return
      end

      M._server_ready[sonarlint_client.id] = true

      local cos = M._co[sonarlint_client.id]
      M._co[ctx.client_id] = {}
      if cos then
         for _, co in ipairs(cos) do
            coroutine.resume(co)
         end
      end
   end
end

local function request_settings(uri)
   local bufnr = vim.uri_to_bufnr(uri)

   local _, settings = require("sonarlint.jdtlsutil").execute_command({
      command = "java.project.getSettings",
      arguments = {
         uri,
         {
            "org.eclipse.jdt.core.compiler.source",
            "org.eclipse.jdt.ls.core.vm.location",
         },
      },
   }, nil, bufnr)

   local vm_location = nil
   local source_level = nil

   if settings then
      vm_location = settings["org.eclipse.jdt.ls.core.vm.location"]
      source_level = settings["org.eclipse.jdt.core.compiler.source"]
   end

   local is_test_file_cmd = {
      command = "java.project.isTestFile",
      arguments = { uri },
   }
   local options
   local is_test
   if vim.startswith(uri, "jdt://") then
      is_test = false
      options = vim.fn.json_encode({ scope = "runtime" })
   else
      local err, is_test_file = require("sonarlint.jdtlsutil").execute_command(is_test_file_cmd, nil, bufnr)
      is_test = is_test_file
      assert(not err, vim.inspect(err))
      options = vim.fn.json_encode({
         scope = is_test_file and "test" or "runtime",
      })
   end
   local cmd = {
      command = "java.project.getClasspaths",
      arguments = { uri, options },
   }
   local err1, resp = require("sonarlint.jdtlsutil").execute_command(cmd, nil, bufnr)
   if err1 then
      vim.notify("Error executing java.project.getClasspaths: " .. err1.message, vim.log.levels.ERROR)
   end

   return {
      projectRoot = (resp or {}).projectRoot,
      sourceLevel = source_level,
      classpath = (resp or {}).classpaths,
      isTest = is_test,
      vmLocation = vm_location,
   }
end

function M.get_java_config_handler(_, file_uri, ctx)
   local uri = type(file_uri) == "table" and file_uri[1] or file_uri

   local sonarlint_client = vim.lsp.get_client_by_id(ctx.client_id)

   if M._server_ready[ctx.client_id] == true then
      M.install_classpath_listener(sonarlint_client)
      return request_settings(uri)
   else
      local pco = coroutine.running()
      local co = coroutine.create(function()
         M.install_classpath_listener(sonarlint_client)
         local resp = request_settings(uri)
         coroutine.resume(pco, resp)
         return resp
      end)

      M._co[ctx.client_id] = { co }

      return coroutine.yield()
   end
end

function M.init_config(sonarlint_client)
   if M._initialized_clients[sonarlint_client.client_id or sonarlint_client.id] == true then
      return
   end
   local client = utils.get_jdtls_client(sonarlint_client)
   if client == nil then
      vim.defer_fn(function()
         M.init_config(sonarlint_client)
      end, 100)
      return
   end
   M._initialized_clients[sonarlint_client.client_id or sonarlint_client.id] = true
   if client.config.handlers["language/status"] then
      local old_handler = client.config.handlers["language/status"]
      client.config.handlers["language/status"] = function(...)
         old_handler(...)
         M.handle_service_ready(...)
      end
   else
      client.config.handlers["language/status"] = M.handle_service_ready
   end
end

return M

local M = {}

---@diagnostic disable-next-line: deprecated
M.get_clients = vim.lsp.get_clients or vim.lsp.get_active_clients

function M.is_open_in_editor(uri)
   for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
      if uri == vim.uri_from_bufnr(bufnr) then
         return true
      end
   end
   return false
end

function M.get_sonarlint_client(jdtls_client)
   local clients = M.get_clients({ name = "sonarlint.nvim" })

   if jdtls_client then
      for _, client in ipairs(clients) do
         if client.config.root_dir == jdtls_client.config.root_dir then
            return client
         end
      end
      return nil
   else
      return clients[1]
   end
end

function M.get_jdtls_client(sonarlint_client)
   for _, client in ipairs(M.get_clients()) do
      local command_provider = client.server_capabilities.executeCommandProvider
      local commands = type(command_provider) == "table" and command_provider.commands or {}
      for _, command in ipairs(commands) do
         if command:sub(1, 5) == "java." then
            if client.config.root_dir == sonarlint_client.config.root_dir then
               return client
            end
         end
      end
   end

   return nil
end

return M
